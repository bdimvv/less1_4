FROM debian:9 as builder


WORKDIR /opt
RUN apt update \
	&& apt upgrade -y \
	&& apt install -y wget build-essential libpcre3 libpcre3-dev libghc-zlib-dev libssl1.0-dev
RUN mkdir ng \
	&& cd ng/ \
	&& mkdir nginx \
	&& mkdir luajit2 \
	&& mkdir luamod \
	&& mkdir ndk \
	&& mkdir rcore \
	&& cd /
RUN cd ng/ \
	&& wget http://nginx.org/download/nginx-1.13.7.tar.gz \
	&& tar -xvf nginx-1.13.7.tar.gz -C ./nginx
RUN cd ng/ \
	&& wget https://github.com/openresty/luajit2/archive/v2.1-20171103.tar.gz \
	&& tar -xvf v2.1-20171103.tar.gz -C ./luajit2
RUN cd ng/ \
	&& wget https://github.com/vision5/ngx_devel_kit/archive/v0.3.0.tar.gz \
	&& tar -xvf v0.3.0.tar.gz -C ./ndk
RUN cd ng/ \
	&& wget https://github.com/openresty/lua-nginx-module/archive/v0.10.11.tar.gz \
	&& tar -xvf v0.10.11.tar.gz -C ./luamod
RUN cd ng/ \
	&& wget https://github.com/openresty/lua-resty-core/archive/v0.1.13.tar.gz \
	&& tar -xvf v0.1.13.tar.gz -C ./rcore
RUN cd ng/ \
	&& rm *tar.gz
RUN cd ng/luajit2/luajit2-2.1-20171103 \
	&& make -j2 && make install
RUN cd ng/rcore/lua-resty-core-0.1.13 \
	&& make && make install
RUN cd ng/nginx/nginx-1.13.7/ \
  && export LUAJIT_LIB=/usr/local/lib \
  && export LUAJIT_INC=/usr/local/include/luajit-2.1 \
  && ./configure --prefix=/opt \
      --prefix=/etc/nginx \
      --sbin-path=/usr/sbin/nginx \
      --modules-path=/usr/lib/nginx/modules \
      --conf-path=/etc/nginx/nginx.conf \
      --error-log-path=/var/log/nginx/error.log \
      --http-log-path=/var/log/nginx/access.log \
      --pid-path=/var/run/nginx.pid \
      --lock-path=/var/run/nginx.lock \
      --http-client-body-temp-path=/var/cache/nginx/client_temp \
      --http-proxy-temp-path=/var/cache/nginx/proxy_temp \
      --http-fastcgi-temp-path=/var/cache/nginx/fastcgi_temp \
      --http-uwsgi-temp-path=/var/cache/nginx/uwsgi_temp \
      --http-scgi-temp-path=/var/cache/nginx/scgi_temp \
      --user=nginx \
      --group=nginx \
      --with-compat \
      --with-file-aio \
      --with-threads \
      --with-http_addition_module \
      --with-http_auth_request_module \
      --with-http_dav_module \
      --with-http_flv_module \
      --with-http_gunzip_module \
      --with-http_gzip_static_module \
      --with-http_mp4_module \
      --with-http_random_index_module \
      --with-http_realip_module \
      --with-http_secure_link_module \
      --with-http_slice_module \
      --with-http_ssl_module \
      --with-http_stub_status_module \
      --with-http_sub_module \
      --with-http_v2_module \
      --with-mail \
      --with-mail_ssl_module \
      --with-stream \
      --with-stream_realip_module \
      --with-stream_ssl_module \
      --with-stream_ssl_preread_module \
      --with-ld-opt='-specs=/usr/share/dpkg/no-pie-link.specs -Wl,-z,relro -Wl,-rpath,/usr/lib/x86_64-linux-gnu,-z,now -Wl,--as-needed -pie' \
      --add-module=/opt/ng/ndk/ngx_devel_kit-0.3.0 \
      --add-module=/opt/ng/luamod/lua-nginx-module-0.10.11 \
  && make -j2 \
  && make install


FROM debian:9


RUN apt-get update \
	&& apt-get install -y libssl1.0.2 \
	&& apt-get purge -y --auto-remove \
	&& rm -rf /var/lib/apt/lists/* \
	&& mkdir /var/log/nginx/ \
	&& touch /var/log/nginx/error.log \
	&& useradd --no-create-home nginx \
	&& mkdir /etc/nginx/ \
	&& mkdir /etc/nginx/html/ \
	&& mkdir /var/cache/nginx/ \
	&& mkdir /var/cache/nginx/client_temp
#COPY ./nginx.conf /etc/nginx/nginx.conf
COPY --from=builder /opt/ng/nginx/nginx-1.13.7/html /etc/nginx/html
COPY --from=builder /usr/sbin/nginx /usr/sbin/nginx
COPY --from=builder /usr/local/lib/libluajit-5.1.so.2.1.0 /usr/lib/x86_64-linux-gnu/libluajit-5.1.so.2
COPY --from=builder /usr/local/lib/lua /usr/local/share/lua
RUN chmod +x /usr/sbin/nginx
CMD ["/usr/sbin/nginx", "-g", "daemon off;"]
